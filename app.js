var express = require("express"),
    bodyParser = require("body-parser"),
    cookieParser = require("cookie-parser"),
    session = require("express-session"),
    OAuth = require("oauth").OAuth;

/**
 * @credentials
 * username bodyticks.test
 * pass DQA501eWRw
 * secret Ia8LuBdYcHO8K7vw0sBIFoGDmrYdKERZedq
 * key 3850a62f-f449-4466-a617-62207bdd87fe
 *
 * @enviorements
 * --test
 *
 *  http://connecttest.garmin.com
 *  http://connecttest.garmin.com/oauthConfirm
 *  http://connectapitest.garmin.com/oauth‐service‐1.0/oauth/access_token
 *  http://connectapitest.garmin.com/oauth‐service‐1.0/oauth/request_token
 *
 *  --production
 *
 *  http://connect.garmin.com
 *  http://connect.garmin.com/oauthConfirm
 *  http://connectapi.garmin.com/oauth‐service‐1.0/oauth/access_token
 *  http://connectapi.garmin.com/oauth‐service‐1.0/oauth/request_token
 */


var app = express();

app.set('env', 'dev');
//app.set('env','dist');
app.use(bodyParser.urlencoded());
app.use(bodyParser.json());
app.use(cookieParser());
app.use(session({secret: "sirakov-garmin"}));


var main_url = "http://connecttest.garmin.com";
var oauth_confirm = "http://connecttest.garmin.com/oauthConfirm";
var oauth_access_token = "http://connectapitest.garmin.com/oauth-service-1.0/access_token";
var oauth_request_token = "http://connectapi.garmin.com/oauth-service-1.0/oauth/request_token";

var oauth = new OAuth(
    'http://connectapitest.garmin.com/oauth-service-1.0/oauth/request_token',
    'http://connectapitest.garmin.com/oauth-service-1.0/oauth/access_token',
    '3850a62f-f449-4466-a617-62207bdd87fe',
    'Ia8LuBdYcHO8K7vw0sBIFoGDmrYdKERZedq',
    '1.0',
    null,
    'HMAC-SHA1'
);
app.get('/', function (req, res) {
    var access_token =  req.session.oauth_access_token;
    var access_token_secret = req.session.oauth_access_token_secret;
    console.log(access_token);
    console.log(access_token_secret);
    oauth.get(
        "http://connecttest.garmin.com",
        req.session.oauth_access_token,
        req.session.oauth_access_token_secret,
        function(error,data, response){
            console.log(data);
            console.log(response.statusCode);
    })
});
app.get('/callback', function (req, res, next) {
    req.session.oauth_authorized_token = req.query.oauth_token;
    req.session.oauth_authorized_verifier = req.query.oauth_verifier;
    console.log(req.session.oauth_authorized_verifier);
    oauth.getOAuthAccessToken(req.session.oauth_authorized_token, req.session.oauth_unauthorized_token_secret, req.session.oauth_authorized_verifier, function (error, oauth_access_token, oauth_access_token_secret, results2) {
        req.session.oauth_access_token = oauth_access_token;
        req.session.oauth_access_token_secret = oauth_access_token_secret;
        res.redirect('/');
    })


    //libLogin.finalLogin(req, res, next);
})
app.get('/login', function (req, res, next) {
    oauth.getOAuthRequestToken(function (error, oauth_token, oauth_token_secret, results) {
        req.session.oauth_unauthorized_token = oauth_token;
        req.session.oauth_unauthorized_token_secret = oauth_token_secret;
        res.redirect("http://connecttest.garmin.com/oauthConfirm?oauth_token=" + oauth_token);
        res.end();
    })
})

app.get('/auth/garmin/callback',function(req, res, next){
    console.log(req.query.oauth_token);
    console.log(req.query.oauth_verifier);
    req.session.oauth_authorized_token = req.query.oauth_token;
    req.session.oauth_authorized_verifier = req.query.oauth_verifier;
    console.log(req.session.oauth_authorized_verifier);
    oauth.getOAuthAccessToken(req.session.oauth_authorized_token, req.session.oauth_unauthorized_token_secret, req.session.oauth_authorized_verifier, function (error, oauth_access_token, oauth_access_token_secret, results2) {
        req.session.oauth_access_token = oauth_access_token;
        req.session.oauth_access_token_secret = oauth_access_token_secret;
        res.redirect('/');
    });
})

app.listen(3005);
